// 2) Створіть функцію, яка перевіряє довжину рядка. 
// Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true,
// якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. 

function ifMax(str, maxLength)
{
    return (str.length > maxLength) ? (false) : (true);
}

let str, maxLength;

str = prompt("Заповни рядок");
maxLength = prompt("ВВеди максимальну кількість символів рядку");

if(ifMax(str, maxLength)) console.log(true);
else console.log(false);


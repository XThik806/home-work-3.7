1) Як можна створити рядок у JavaScript?

За допомогою літералів рядків, які заключаються в подвійні або одинарні лапки, наприклад: let str = "Hello, world!";

За допомогою конструктора String(), який приймає будь-яке значення як аргумент і перетворює його в рядок, наприклад: let str = String(42);

За допомогою шаблонних рядків, які заключаються в зворотні лапки і дозволяють використовувати вирази всередині рядків, наприклад: let name = "Alice"; let str = Hello, ${name}!;

2) Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?

Одинарні та подвійні лапки в JavaScript функціонують однаково і дозволяють створювати рядки з будь-якими символами, окрім тих, що використовуються для закриття рядка. Зворотні лапки в JavaScript дозволяють створювати шаблонні рядки, які можуть включати вирази всередині рядка за допомогою синтаксису ${вираз}

3) Як перевірити, чи два рядки рівні між собою?

Достатньо порівняти їх із використанням ===

4) Що повертає Date.now()?

Date.now() повертає кількість мілісекунд, що минули з 1 січня 1970 року 00:00:00 UTC

5) Чим відрізняється Date.now() від new Date()?

Date.now() повертає поточну часову мітку в мілісекундах, а new Date() створює новий об’єкт Date, який може представляти будь-яку дату та час.
// 1) Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

function isPalindrome(str, revStr)
{
    
    if(str.length === 1) return false;
    if(str === revStr) return true;
    else return false;
}

let str, revStr;

str = prompt("Введіть рядок").replaceAll(" ", "").toLowerCase();
revStr = str.split("").reverse().join("").toLowerCase();

if (isPalindrome(str, revStr) === true) console.log(true);
else console.log(false)

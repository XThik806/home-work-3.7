// 3) Створіть функцію, яка визначає скільки повних років користувачу. 
// Отримайте дату народження користувача через prompt. 
// Функція повина повертати значення повних років на дату виклику функцію.

function getAge(birthDate) {
    let today = new Date();
    let birth = new Date(birthDate);
    let age = today.getFullYear() - birth.getFullYear();
    let month = today.getMonth() - birth.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < birth.getDate())) {
      age--;
    }
    return age;
  }
  
  let userBirthDate = prompt("Введіть вашу дату народження у форматі рррр-мм-дд");

  console.log("Вам " + getAge(userBirthDate) + " років");
  